class ChangeMicropostsPictureNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :microposts, :picture, :string, false
  end
end
