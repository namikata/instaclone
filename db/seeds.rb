User.create!(name:  'Example User',
             nickname: 'example',
             email: 'example@railstutorial.org',
             password:              'foobar',
             password_confirmation: 'foobar',
             sex: 'woman',
             introduce: 'hogehoge',
             admin: true)

99.times do |n|
  name  = "hoge#{n + 1}"
  email = "example-#{n + 1}@railstutorial.org"
  password = '123123'
  nickname = Faker::Pokemon.name
  User.create!(name:  name,
               nickname: nickname,
               email: email,
               password:              password,
               password_confirmation: password)
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each do |user|
    user.microposts.create!(content: content,
                            picture: File.open('./app/assets/images/kitten.jpg'))
  end
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
