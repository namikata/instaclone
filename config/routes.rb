Rails.application.routes.draw do
  

  root 'top_pages#home'

  devise_scope :user do
    get     '/signup',         to: 'users/registrations#new'
    post    '/signup',         to: 'users/registrations#create'
    get     '/users/edit',     to: 'users/registrations#edit'
    patch   '/users/:id',      to: 'users/registrations#update'
    delete  '/users/:id',      to: 'users/registrations#delete'
    get     'login',           to: 'users/sessions#new'
    post    '/login',          to: 'users/sessions#create'
    delete  '/logout',         to: 'users/sessions#destroy'
    get     '/password/reset/new', to: 'users/passwords#new'
    post    'user/password',       to: 'users/passwords#create'
    get     'users/password/edit', to: 'users/passwords#edit'
    patch   'users/password', to: 'users/passwords#update'
    
  end
  
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    passwords: 'users/passwords', 
    omniauth_callbacks: 'users/omniauth_callbacks' 
  } 
  
  resources :users do
    member do
      get :following, :followers
      get :like
    end
  end
  
  resources :users,         only: [:index, :show, :create, :edit, :destroy]
  resources :microposts,    only: [:create, :destroy] do
    resources :comments, only: [:new, :create, :destroy]
  end
  resources :relationships, only: [:create, :destroy]
  resources :likes,         only: [:create, :destroy]
  
end
