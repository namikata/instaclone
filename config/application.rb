require_relative 'boot'
require 'rails/all'

Bundler.require(*Rails.groups)

module InstaApp
  class Application < Rails::Application
    config.time_zone = 'Tokyo'
    config.active_record.default_timezone = :local
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]
    # 　以下の記述を追記する(設定必須)
    # デフォルトのlocaleを日本語(:ja)にする

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.i18n.default_locale = :ja
    config.generators do |g|
      g.test_framework :rspec,
                       view_specs: false,
                       routing_specs: false
    end
    config.action_view.embed_authenticity_token_in_remote_forms = true
  end
end
