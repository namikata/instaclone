module SessionsHelper
  def log_in(user)
    session[:user_id] = user.id
  end

  def login_user
    @current_user ||= User.find_by(id: session[:user_id]) if session[:user_id]
  end

  def logged_in?
    !session[:user_id].nil?
  end

  def current_user?(user)
    user == current_user
  end

  def log_out
    session.delete(:user_id)
    @currentd_user = nil
  end
end
