class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :destroy, :following, :followers]
  before_action :correct_user,   only: [:edit]
  before_action :admin_user,     only: [:destroy]

  def index
    @microposts = Micropost.new
    @users = User.paginate(page: params[:page])
    @users = @users.get_by_name params[:nickname] if params[:nickname].present?
  end

  def show
    @user = User.find(params[:id])
    @microposts = Micropost.new
    @microposts = @user.microposts.paginate(page: params[:page])
    @microposts = @microposts.get_by_content params[:content] if params[:content].present?
  end

  def create
    @user = User.new(params_user)
    if @user.save
      flash[:notice] = '更新しました'
      log_in(@user)
      sign_in(@user)
      redirect_to @user
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def destroy
    User.find_by(id: params[:id]).destroy
    flash[:success] = 'ユーザーを消しました'
    redirect_to users_url
  end

  def following
    @title = 'Following'
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = 'Followers'
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  def like
    @user = User.find_by(id: params[:id])
    @microposts = @user.microposts
    @likes = Like.where(user_id: @user.id)
  end

  private

  def params_user
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :nickname)
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

  def admin_user
    redirect_to(login_path) unless current_user.admin?
  end
end
