class TopPagesController < ApplicationController
  def home
    if logged_in?
      @micropost = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
    @users = @users.get_by_name params[:nickname] if params[:nickname].present?
    end
end
