class ApplicationController < ActionController::Base
  include SessionsHelper
  before_action :configure_permitted_parameters, if: :devise_controller?

  def store_location
    if request.fullpath != new_user_registration_path &&
       request.fullpath != new_user_session_path &&
       # request.fullpath != "/users/password" &&
       request.fullpath !~ Regexp.new('\\A/users/password.*\\z') &&
       !request.xhr?
      session[:previous_url] = request.fullpath
    end
  end

  def after_sign_in_path_for(resource)
    if session[:previous_url] == root_path
      super
    else
      session[:previous_url] || root_path
    end
  end

  def update_resource(_resource, params)
    if params[:password].blank? && params[:password_confirmation].blank? && params[:current_password].blank?
      @user.update_without_password(params)
    else
      @user.update_with_password(params)
    end
  end
  end
def correct_user
  @user = User.find(params[:id])
  redirect_to(root_url) unless current_user?(@user)
end

def after_sign_out_path_for(_user)
  login_path # ログアウト後に遷移するpathを設定
end

def after_sign_in_path_for(_user)
  users_path # ログアウト後に遷移するpathを設定
end

# 登録時のスキップ機能
def skip_confirmation!
  self.confirmed_at = Time.now.utc
end
private

def skip_reconfirmation!
  @bypass_confirmation_postpone = true
end

def logged_in_user
  unless user_signed_in?
    flash[:dander] = 'ログインまたは新規登録をしてください'
    redirect_to login_url
  end
end

def search
  @users = User.where('nickname ?', params[:nickname]).order(created_at: 'DESC')
  @microposts = Micropost.where('content ?', params[:content]).order(created_at: 'DESC')
end

def configure_permitted_parameters
  devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
end

def configure_account_update_params
  devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  devise_parameter_sanitizer.permit(:account_update, keys: [:sex])
  devise_parameter_sanitizer.permit(:account_update, keys: [:introduce])
end
