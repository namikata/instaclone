class Users::SessionsController < Devise::SessionsController
  before_action :logged_in_user, only: [:edit, :update]

  def new
    @user = User.new
  end

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.valid_password?(params[:session][:password])
      user.skip_reconfirmation!
      log_in(user)
      redirect_to user
      sign_in(user)
      flash[:notice] = 'ログインしました'
    else
      flash.now[:danger] = 'アドレスとパスワードが一致しません'
      render 'new'
    end
  end

  # DELETE /resource/sign_out
  def destroy
    super
    session[:keep_signed_out] = true
  end

  protected

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

  def skip_reconfirmation!
    @bypass_confirmation_postpone = true
  end
end
