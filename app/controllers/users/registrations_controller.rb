class Users::RegistrationsController < Devise::RegistrationsController
  before_action :logged_in_user, only: [:edit, :update]
  before_action :configure_account_update_params, only: [:update]

  def new
    @user = User.new
  end

  def create
    @user = User.new(params_user)
    @user.skip_confirmation!
    if @user.save
      log_in(@user)
      sign_in(@user)
      flash[:notice] = 'アカウントを登録しました'
      redirect_to @user
    else
      render 'new'
    end
  end

  private

  def params_user
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :nickname, :sex, :introduce)
  end
end
