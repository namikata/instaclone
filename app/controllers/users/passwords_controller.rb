# frozen_string_literal: true

class Users::PasswordsController < Devise::PasswordsController
  def new
    super
  end

  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(params_user)
    yield resource if block_given?

    if successfully_sent?(resource)
      respond_with({}, location: after_sending_reset_password_instructions_path_for(resource_name))
      flash[:notice] = 'メールをご確認ください'

    else
      render 'new'
    end
  end

  def edit
    self.resource = resource_class.new
    set_minimum_password_length
    resource.reset_password_token = params[:reset_password_token]
  end

  def update
    self.resource = resource_class.reset_password_by_token(params_user)
    yield resource if block_given?

    if resource.errors.empty?
      resource.unlock_access! if unlockable?(resource)
      if Devise.sign_in_after_reset_password
        flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
        flash[:notice] = 'パスワードを変更しました'
        resource.after_database_authentication
        sign_in(resource_name, resource)
        skip_reconfirmation!
        log_in @user
      else
        set_flash_message!(:notice, :updated_not_active)
        render 'edit'
      end
      respond_with resource, location: after_resetting_password_path_for(resource)
    else
      set_minimum_password_length
      respond_with resource
    end
  end

  private

  def params_user
    params.require(:user).permit(:email, :password, :password_confirmation, :reset_password_token)
  end

  def send_reset_password_instructions(attributes = {})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    recoverable.send_reset_password_instructions if recoverable.persisted?
    recoverable
  end
end
