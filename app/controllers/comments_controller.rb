class CommentsController < ApplicationController
  def new
    @micropost = Micropost.find(params[:micropost_id])
    @comment = Comment.new
  end

  def create
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.build(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash[:success] = 'コメントしました'
      redirect_to request.referrer || root_url
    else
      flash[:success] = 'コメントできませんでした'
      render 'new'
  end
  end

  def destroy
    @micropost = Micropost.find(params[:micropost_id])
    @comment = Comment.find(params[:id])
    @comment.destroy
    redirect_to request.referrer || root_url
  end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end
end
