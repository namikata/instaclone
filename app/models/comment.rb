class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :micropost
  validates :content, length: { maximum: 140 }
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :micropost_id, presence: true
end
