class Like < ApplicationRecord
  belongs_to :micropost
  belongs_to :user
  counter_culture :micropost
  validates_uniqueness_of :micropost_id, scope: :user_id
  validates :user_id, presence: true
  validates :micropost_id, presence: true
end
