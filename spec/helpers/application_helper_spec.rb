require 'rails_helper'
include ApplicationHelper

RSpec.describe ApplicationHelper, type: :helper do
  describe '#full_title' do
    context 'page_title が空だったら' do
      it 'タイトルだけ' do
        expect(helper.full_title('')).to eq('INSTACLONE')
      end
    end

    context '空じゃなかったら' do
      it 'アクションネームとタイトルが入る' do
        expect(helper.full_title('hoge')).to eq('hoge | INSTACLONE')
      end
    end
  end
end
