require 'rails_helper'
RSpec.describe Devise::SessionsController, type: :system do
  include TestHelper

  describe 'ログイン前' do
    let(:user) { FactoryBot.create(:tom) }

    context 'フォーム正常' do
      pending 'ログイン成功' do
        visit login_path
        fill_in 'session[email]', with: user.email
        fill_in 'session[password]', with: user.password
        click_button 'ログイン'
        expect(is_logged_in?).to be_truthy
        expect(current_path).to eq users_path(user)
        expect(page).to have_content 'ログインしました'
      end

      it 'ユーザーのログアウトのテスト' do
        sign_in_as(user)
        click_link 'ログアウト'
        expect(page).to have_content 'ログアウトしました'
        expect(page).to have_link 'サインイン'
      end

      it 'ログインしていないときはeditパスにいけない' do
        visit users_edit_path(user)
        expect(page).to have_content 'アカウント登録もしくはログインしてください。'
        visit root_path
      end
    end

    context 'アドレス未入力' do
      it 'ログイン失敗' do
        visit login_path
        fill_in 'session[email]', with: ''
        fill_in 'session[password]', with: user.password
        click_button 'ログイン'
        expect(current_path).to eq login_path
        expect(page).to have_content 'アドレスとパスワードが一致しません'
      end
    end

    context 'アドレスとパスワードが違う' do
      it 'ログイン失敗' do
        visit login_path
        fill_in 'session[email]', with: 'hogehogehoge@example.com'
        fill_in 'session[password]', with: 'fooooobar'
        click_button 'ログイン'
        expect(current_path).to eq login_path
        expect(page).to have_content 'アドレスとパスワードが一致しません'
      end
    end
  end
end
