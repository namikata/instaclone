require 'rails_helper'
RSpec.describe Devise::Users, type: :system do
  include TestHelper

  let(:user) { FactoryBot.build(:tom) }
  context 'Userプロフィール画面に対するテスト' do
    pending '表示される' do
      sign_in user
      visit users_path(user)
      expect(page).to have_content 'いいね一覧'
    end
  end
end
