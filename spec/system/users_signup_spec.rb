require 'rails_helper'
RSpec.describe Devise::SessionsController, type: :system do
end

describe '新規登録' do
  context 'フォーム正常成功' do
    it '新規登録成功' do
      visit signup_path
      fill_in 'user[nickname]', with: 'tom'
      fill_in 'user[name]', with: 'tom'
      fill_in 'user[email]', with: 'hogehoge@examole.com'
      fill_in 'user[password]', with: 'foobar'
      fill_in 'user[password_confirmation]', with: 'foobar'
      click_button '登録'
      user = User.first
      expect(current_path).to eq user_path(user)
      expect(page).to have_content 'アカウントを登録しました'
    end

    context '未入力有り登録失敗' do
      it '新規登録失敗' do
        visit signup_path
        fill_in 'user[nickname]', with: ''
        fill_in 'user[name]', with: ''
        fill_in 'user[email]', with: 'hogehoge@examole.com'
        fill_in 'user[password]', with: 'foo'
        fill_in 'user[password_confirmation]', with: 'bar'
        click_button '登録'
        user = User.first
        expect(current_path).to eq signup_path
        expect(page).to have_content '保存されませんでした'
      end
    end

    context '登録済みアドレス' do
      let!(:user) { FactoryBot.create(:tom) }
      it '新規登録失敗' do
        visit signup_path
        fill_in 'user[nickname]', with: user.nickname
        fill_in 'user[name]', with: user.name
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: 'foobar'
        fill_in 'user[password_confirmation]', with: 'foobar'
        click_button '登録'
        user = User.first
        expect(current_path).to eq signup_path
        expect(page).to have_content '保存されませんでした'
      end
    end
  end
end
