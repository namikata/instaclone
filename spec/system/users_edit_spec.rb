require 'rails_helper'
RSpec.describe Devise::SessionsController, type: :system do
  include TestHelper

  describe 'ユーザー編集ページ' do
    let!(:user) { FactoryBot.create(:tom) }

    context 'フォーム正常' do
      before do
        sign_in_as(user)
        visit edit_user_registration_path(user)
        fill_in 'user[name]', with: user.name
        fill_in 'user[nickname]', with: user.nickname
        fill_in 'user[email]', with: user.email
        fill_in 'user[current_password]', with: user.password
        fill_in 'user[password]', with: 'foobab'
        fill_in 'user[password_confirmation]', with: 'foobab'
      end
      it '成功する' do
        click_on '完了'
        expect(page).to have_content 'アカウント情報を変更しました。'
      end
    end

    context 'フォーム未入力あり' do
      before do
        sign_in_as(user)
        visit edit_user_registration_path(user)
        fill_in 'user[name]', with: user.name
        fill_in 'user[nickname]', with: user.nickname
        fill_in 'user[email]', with: user.email
        fill_in 'user[current_password]', with: ''
        fill_in 'user[password]', with: ' '
        fill_in 'user[password_confirmation]', with: user.password
      end
      it '失敗する' do
        click_on '完了'
        expect(page).to have_content 'プロフィール編集ページ'
      end
    end

    pending 'フレンドリーフォアリング' do
      visit edit_user_path(user)
      expect(page).to have_content 'ログインまたは新規登録をしてください'
      fill_in 'session[email]', with: user.email
      fill_in 'session[password]', with: user.password
      click_button 'ログイン'
      save_and_open_page
      expect(page).to have_content 'プロフィール編集ページ'
    end
  end
end
