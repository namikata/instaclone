require 'rails_helper'
RSpec.describe Devise::SessionsController, type: :system do
  include TestHelper

  let(:admin) { FactoryBot.create(:tom) }
  let(:nonadmin) { FactoryBot.create(:micher) }
  it 'ページネーションを含めたUsersindexのテスト' do
    sign_in_as(admin)
    visit users_path
    expect(page).to have_content 'ユーザー一覧'
    User.paginate(page: 1).each do |user|
      expect(page).to have_css('li', text: user.nickname)
    end
  end
end
