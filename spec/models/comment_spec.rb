require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:comment) { FactoryBot.build(:comment) }
  it 'コメントが140文字以内であるか' do
    comment.content = 'a' * 141
    expect(comment).to_not be_valid
  end
  it 'コメントのuser_idがないと無効であるか' do
    comment.user_id = nil
    expect(comment).to_not be_valid
  end
end
