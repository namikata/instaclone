require 'rails_helper'

RSpec.describe Micropost, type: :model do
  let(:micropost) { FactoryBot.build(:orange) }

  pending 'マイクロポストのuser_idがないと無効であるか' do
    micropost.user_id = nil
    expect(micropost).to be_valid
  end

  pending 'マイクロポストが空だと無効であるか' do
    micropost.content = ' '
    expect(micropost).to_not be_valid
  end

  pending 'マイクロポストが140文字以内であるか' do
    micropost.content = 'a' * 141
  end

  pending 'pictureがnilだと無効であるか' do
    micropost.picture = nil
    expect(micropost).to_not be_valid
  end
end
