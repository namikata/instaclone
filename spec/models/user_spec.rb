require 'rails_helper'
include TestHelper

def follow(other_user)
  following << other_user
end

# ユーザーのフォローを解除する
def unfollow
  active_relationships.find_by(followed_id: other_user.id).destroy
end

# 現在のユーザーがフォローしてたらtrueを返す
def following?(other_user)
  following.include?(other_user)
end

RSpec.describe User, type: :model do
  let(:user)    { build(:tom) }
  let(:michael) { build(:michael) }

  it 'サンプルデータは正しいか' do
    expect(user).to be_valid
  end

  it '名前が空白ではないか' do
    user.name = ' '
    expect(user).not_to be_valid
  end

  it 'emailが空ではないか' do
    user.email = ' '
    expect(user).not_to be_valid
  end
  it 'nicknameが空ではないか' do
    user.nickname = ' '
    expect(user).not_to be_valid
  end
  it 'nicknameが50文字以内か' do
    user.nickname = 'a' * 51
    expect(user).to be_invalid
  end

  it '名前が５０文字以内か' do
    user.name = 'a' * 51
    expect(user).to be_invalid
  end

  it 'emailが２５５文字以内か' do
    user.email = 'a' * 255
    expect(user).to be_invalid
  end
  it 'emailがユニークであるか' do
    user.email = User.create(email: 'foobar@email.com')
    user.email.upcase
    user.save
    expect(user).not_to be_valid
  end

  it 'メールの検証で無効なアドレスを拒否する必要がある' do
    addresses = %w[user@foo,com user_at_foo.org example.user@foo. foo@bar_baz.com foo@bar+baz.com]
    addresses.each do |invalid_addres|
      user = FactoryBot.build(:tom, email: invalid_addres)
      expect(user).to be_invalid
    end
  end
  it 'emailを小文字に変更後の値が大文字を混ぜて登録されたアドレスと同じか' do
    user.email = 'Foo@ExAMPle.Cpo'
    user.save!
    expect(user.reload.email).to eq 'foo@example.cpo'
  end

  it 'passwordが空ではない' do
    user.password = user.password_confirmation = ' ' * 6
    expect(user).not_to be_valid
  end
  context 'passwordが６文字である' do
    it '正しい' do
      user.password = user.password_confirmation = 'a' * 6
      expect(user).to be_valid
    end

    it '正しくない' do
      user.password = user.password_confirmation = 'a' * 5
      expect(user).not_to be_valid
    end
  end
end
