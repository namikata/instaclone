require 'rails_helper'

RSpec.describe Relationship, type: :model do
  include TestHelper

  let(:user) { FactoryBot.create(:micher) } # フォローしている
  let(:other_user) { FactoryBot.create(:lana) } # フォローされている
  let(:active_relationship) { user.active_relationships.build(followed_id: other_user.id) }
  let(:relationship) do
    Relationship.new(follower_id: user.id,
                     followed_id: other_user.id)
  end

  it 'relationshipは有効である' do
    expect(relationship).to be_valid
  end

  it 'follower_idがnilの場合は無効' do
    relationship.follower_id = nil
    expect(relationship).to_not be_valid
  end

  it 'followed_idがnilの場合は無効' do
    relationship.followed_id = nil
    expect(relationship).to_not be_valid
  end

  it 'userがother_userをfollowしている' do
    user.following?(other_user)
    expect(user.follow(other_user)).to be_truthy
  end

  pending 'followingではない' do
    other_user = Relationship.find(params[:id]).followed
    user.unfollow(other_user)
    user.following?(other_user)
    expect(user.follow(other_user)).to be_falsey
    　
  end
end
