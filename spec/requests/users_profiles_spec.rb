require 'rails_helper'

RSpec.describe 'UsersProfiles', type: :request do
  describe 'GET /users_profiles' do
    let(:user) { FactoryBot.build(:tom) }
    context 'ログインしていない' do
      it 'login_pathにリダイレクトされる' do
        post microposts_path, params: { micropost: { content: 'Lorem ipusam' } }
        expect(response).to redirect_to '/login'
      end
      it 'login_pathにリダイレクトされる' do
        delete micropost_path(user)
        expect(response).to redirect_to '/login'
      end
    end
  end
end
