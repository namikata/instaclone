require 'rails_helper'

RSpec.describe 'RelationshipsControllers', type: :request do
  let(:one) { FactoryBot.create(:one, follower: micher) }

  describe 'GET /relationships_controllers' do
    context 'ログインしていない時' do
      it 'post relationships_pathにアクセスするとlogin_pathにリダイレクトされる' do
        post relationships_path
        expect(response).to redirect_to login_path
      end

      pending 'delete relationship_pathにアクセスするとlogin_pathにリダイレクトされる' do
        delete relationship_path(micher)
        expect(response).to redirect_to login_pat
      end
    end
  end
end
