require 'rails_helper'
RSpec.describe 'UsersEdits', type: :request do
  include TestHelper
  context 'ユーザーがログインしていない時' do
    let(:user) { FactoryBot.create(:tom) }

    it 'updateをするとlogin_pathにリダイレクトされる' do
      patch users_edit_path, params: { user: { name: user.name,
                                               email: user.email } }

      expect(response).to redirect_to '/users/sign_in'
    end
  end
end
