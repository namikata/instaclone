require 'rails_helper'

RSpec.describe 'Microposts', type: :request do
  include TestHelper

  let(:user) { FactoryBot.build(:tom) }
  let(:user2) { FactoryBot.build(:micher) }
  let(:micropost) { FactoryBot.build(:orange) }

  describe 'GET /microposts' do
    context 'ユーザーを消した時' do
      it 'マイクロポストも一緒に消される' do
        expect do
          user.microposts.build!(content: 'Lorem ipusum')
          user.destroy
        end
        expect(Micropost.where(id: 1).count).to eq 0
      end
    end
  end
  context 'admin以外がmicropostを削除しようとした時' do
    pending 'root_pathにリダイレクトされる' do
      sign_in(user)
      micropost = user.microposts.build(content: 'hello!!!', user_id: 1)
      sign_in(user2)
      micropost.destroy
      expect(response).to redirect_to login_path
    end
  end
end
