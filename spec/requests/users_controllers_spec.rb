require 'rails_helper'

RSpec.describe 'UsersControllers', type: :request do
  include TestHelper

  let(:user) { FactoryBot.create(:tom) }
  let(:otheruser) { FactoryBot.create(:micher) }

  describe 'ログインしていない場合' do
    context 'indexアクション' do
      it 'loginパスにリダイレクトされる' do
        get users_path
        expect(response).to redirect_to login_path
      end
    end

    context 'destroyアクション管理者権限のテスト' do
      pending 'ログイン画面にリダイレクトされる' do
        delete users_path(user)
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'ログインしている場合' do
    pending '他のユーザーログインしてがユーザーの削除をする' do
      it 'できない' do
        sign_in_as otheruser
        delete users_path(user)
      end
    end
  end
end
