require 'rails_helper'

RSpec.describe 'UsersSignups', type: :request do
  describe 'GET /users_signups' do
    it '有効な登録のユーザー' do
      get signup_path
      expect do
        post signup_path, params: { user: { nickname: 'kana',
                                            name: 'ExampleUser',
                                            email: 'user@example.com',
                                            password: '123123',
                                            password_confirmation: '123123' } }
      end
        .to change(User, :count).by(1)
    end

    it '有効な登録のユーザー' do
      get signup_path
      expect do
        post signup_path, params: { user: { nickname: 'kana',
                                            name: 'ExampleUser',
                                            email: 'user@example.com',
                                            password: ' ',
                                            password_confirmation: '123123' } }
      end
        .to change(User, :count).by(0)
    end
  end
end
