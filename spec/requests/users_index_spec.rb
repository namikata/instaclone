require 'rails_helper'

RSpec.describe 'UsersIndex', type: :request do
  let(:admin) { FactoryBot.create(:tom) }
  let(:nonadmin) { FactoryBot.create(:micher) }

  describe 'ログインしていない場合' do
    context 'adminでない人がログインする時' do
      it 'deleteは現れない' do
        sign_in(nonadmin)
        get users_path(nonadmin)
        expect(page).to have_selector 'a', text: 'delete', count: 0
      end
    end
  end
  describe 'ログインしている場合' do
    context 'adminである' do
      pending 'nonadminを消したら１人減る' do
        expect do
          sign_in(admin)
          delete user_path(nonadmin)
        end.to change(User, :count).by(-1)
      end
    end
  end
end
