require 'rails_helper'

RSpec.feature 'UsersEdit', type: :feature do
  include TestHelper
  # user = FactoryBot.create(:tom)
  scenario '編集の成功に対するテスト' do
    user = FactoryBot.create(:tom)
    sign_in_as(user)
    visit edit_user_registration_path(user)

    fill_in 'user[nickname]', with: user.nickname
    fill_in 'user[name]', with: user.name
    fill_in 'user[email]', with: user.email
    fill_in 'user[current_password]', with: user.password
    fill_in 'user[password]', with: 'foobab'
    fill_in 'user[password_confirmation]', with: 'foobab'
    click_on '完了'
    expect(page).to have_content 'アカウント情報を変更しました'
  end

  it '編集の失敗に対するテスト' do
    user = FactoryBot.create(:tom)
    sign_in_as(user)
    visit users_edit_path(user)

    fill_in 'user[nickname]', with: user.nickname
    fill_in 'user[name]', with: user.name
    fill_in 'user[email]', with: user.email
    fill_in 'user[current_password]', with: ''
    fill_in 'user[password]', with: ''
    fill_in 'user[password_confirmation]', with: user.password
    click_on '完了'
    expect(page).to have_content '保存されませんでした'
  end
end
