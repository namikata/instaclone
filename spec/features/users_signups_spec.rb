require 'rails_helper'

RSpec.feature 'UsersSignups', type: :feature do
  scenario 'signup_pathでユーザー登録成功する' do
    @user = User.new(name: '山田', email: 'foo@example.com', password: '123456', nickname: 'TOM')
    expect  do
      visit root_path
      click_link '新規登録'
      fill_in 'user[name]', with: @user.name
      fill_in 'user[nickname]', with: @user.nickname
      fill_in 'user[email]', with: @user.email, count: 2
      fill_in 'user[password]', with: @user.password
      fill_in 'user[password_confirmation]', with: @user.password
      click_button '登録'
      expect(page).to have_content '登録しました'
    end.to change(User, :count). by(1)
  end

  scenario 'signup_pathでユーザー登録失敗する' do
    expect  do
      @user = User.new(name: ' ', email: 'foo@example.com', password: '123456', nickname: 'TOM')
      visit root_path
      click_link '新規登録'
      fill_in 'user[name]', with: @user.name
      fill_in 'user[nickname]', with: @user.nickname
      fill_in 'user[email]', with: @user.email
      fill_in 'user[password]', with: @user.password
      fill_in 'user[password_confirmation]', with: @user.password
      click_button '登録'
      expect(page).to have_selector('#error_explanation')
      expect(page).to have_current_path '/signup'
    end.to change(User, :count). by(0)
  end
end
