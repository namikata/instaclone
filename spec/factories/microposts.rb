FactoryBot.define do
  factory :orange, class: :Micropost do
    content { 'I just ate an orange!' }
    user_id { 1 }
    created_at { 10.minutes.ago }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.jpg')) }
    association :user, factory: :tom
  end
  factory :tau_manifesto, class: :Micropost do
    content { 'Check on the @tauday site by @mhartl:http://tauday.com!' }
    user_id { 1 }
    created_at { 3.years.ago }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.jpg')) }
  end
  factory :cat_video, class: :Micropost do
    content { 'sad cats are sad: http://youtube/PKffmeaer!' }
    user_id { 1 }
    created_at { 2.hours.ago }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.jpg')) }
  end
  factory :most_recent, class: :Micropost do
    content { 'Writing short text' }
    user_id { 1 }
    created_at { Time.zone.now }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.jpg')) }
  end

  factory :faker_micropost, class: :Micropost do
    micropost { n.to_s }
    content { Faker::Lorem.sentence(5) }
    created_at { 42.days.ago }
    picture { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/test.jpg')) }
  end
end
