FactoryBot.define do
  factory :one, class: :Relationship do
    follower { micher }
    followed { lana }
  end

  factory :two, class: :Relationship do
    follower { micher }
    followed { mary }
  end

  factory :three, class: :Relationship do
    follower { lana }
    followed { micher }
  end
  factory :four, class: :Relationship do
    follower { archer }
    followed { micher }
  end
end
