FactoryBot.define do
  factory :tom, class: :User do
    name  { 'トム' }
    email { 'foobar@email.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
    nickname { 'TOM' }
    id { '1' }
    confirmed_at { Time.now } 
  end
  factory :micher, class: :User do
    name  { 'マイケル' }
    email { ' michael@example.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
    nickname { 'MICHAEL' }
    admin { 'true' }
    confirmed_at { Time.now } 
  end
  
  factory :lana, class: :User do
    name  { 'ラナ' }
    email { ' lana@example.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
    nickname { 'LANA' }
    confirmed_at { Time.now } 
  end
  
  factory :mary, class: :User do
    name  { 'マリー' }
    email { ' mary@example.com' }
    password { 'foobar' }
    password_confirmation { 'foobar' }
    nickname { 'MARY' }
    confirmed_at { Time.now } 
  end
  
  factory :faker_user do
    name { Faker::Name.name }
    email { Faker::Internet.email }
  end
end
