FactoryBot.define do
  factory :comment do
    content { 'MyString' }
    user_id :user_id
    micropost_id :micropost_id
  end
end
