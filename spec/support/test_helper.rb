module TestHelper
  def sign_in_as(user)
    # user = User.create(name: "tom",nickname:"tom",email:"hogehoge1@email.com",password: "foobar")
    visit login_path
    fill_in 'session[email]', with: user.email
    fill_in 'session[password]', with: user.password
    user.skip_reconfirmation!
    click_button 'ログイン'
    expect(page).to have_content 'ログインしました'
  end

  def is_logged_in?
    !page.get_rack_session_key('user_id').nil?
   end

  def logout(_user)
    click_on 'ログアウト'
    expect(page).to have_content 'ログアウトしました。'
  end

  def logged_in_user
    unless user_signed_in?
      store_location
      flash[:dander] = 'ログインまたは新規登録をしてください'
      redirect_to login_url
    end
  end

  def skip_reconfirmation!
    @bypass_confirmation_postpone = true
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

  def log_in(user)
    session[:user_id] = user.id
 end

  def logged_in?
    !session[:user_id].nil?
  end

  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end
end
